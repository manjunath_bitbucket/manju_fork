/**
 * Class containing tests for SiteRegisterController
 */
@IsTest public with sharing class SiteRegisterControllerTest {
    @IsTest(SeeAllData=true) static void testRegistration() {
        SiteRegisterController controller = new SiteRegisterController();
        controller.username = 'test@force.com';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);    
        
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd123';
		Profile pguest = [select id from profile where name ='LCBS_BuildingApproveReject Profile' limit 1];
        User userquest = new User(Username ='Username2@username.com', Lastname ='LastName2', Email='xxx@xxx.cxom', 
                      Alias='Alias', CommunityNickname='NICK2', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US',
                        EmailEncodingKey='UTF-8', ProfileId= pguest.Id , LanguageLocaleKey='en_US');
        insert(userquest);
        system.runas(userquest){
        System.assert(controller.registerUser() == null);  
        }
    }
}