/*********************************************
    Class Name       : LCBS_UtilityClass
    Created On       : 
    Created By       : Chiranjeevi Gogulakonda
    Purpose          : Error Message Handling for LCBS Webservice classes.
                       Basically when there is exception on the Apex call we are recording the error info to this object.  
    Test Class URL   : 
*********************************************/


Public Class LCBS_UtilityClass{

    // Log handler: log values to be inserted here. 
    
Public Static void Callouttimes(String ErrorFrom,string UserId,string comment,string QuriesCount,Datetime Reqtime,Datetime Resptime,Decimal Difftime)
    {

LCBS_System_Error_Logs__c LBSlog = new LCBS_System_Error_Logs__c ();

           LBSlog.Apex_Class_Name__c = ErrorFrom;
           LBSlog.Calling_User__c = UserId;
           LBSlog.Comments__c = comment;
           LBSlog.Queries_Count__c = QuriesCount;
           LBSlog.Callout_Request_time__c = ReqTime;
           LBSlog.Callout_Response_time__c = RespTime;
           LBSlog.Callout_Req_and_Res_diff_time_mins__c = DiffTime;

    insert LBSlog; 
    
    }    
    }