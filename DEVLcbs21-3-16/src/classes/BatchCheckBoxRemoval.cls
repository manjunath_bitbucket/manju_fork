global class BatchCheckBoxRemoval implements Database.Batchable<sObject>{

    global final string query;

    global BatchCheckBoxRemoval(){
        query = 'Select Id, Name,Date_EULA_Accepted__c,EULA_Accepted__c,EULA_Rejected__c,Distributor_Invited__c From Account';        
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope) {
              List<Account> updateObjs = new List<Account>();
        
              for(Sobject s : scope) {
                Account obj = (Account) s;
                obj.EULA_Accepted__c= false;
                updateObjs.add(obj);
              }  
              update updateObjs;
    }
    
    global void finish(Database.BatchableContext BC) {
              system.debug('Batch Job to remove check boxes is complete!');
    }   
}