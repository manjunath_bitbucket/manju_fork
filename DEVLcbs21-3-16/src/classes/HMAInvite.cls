public class HMAInvite
{
public String AcctId{get;set;}
public contact cc {get;set;}
public String PAGE_CSS { get; set; }
public user u {get;set;}
public Account c {get;set;}
public document sign {get;set;}
String Env;
public string executive_name {get;set;}
public string executive_phone {get;set;}
public string executive_email {get;set;}
public string url {get;set;}
public string orgid {get;set;}
public string sfdcBaseURL{get;set;}
public Account accnt {get;set;}
 public HMAInvite()
 {
  AcctId = ApexPages.CurrentPage().getparameters().get('id');
  system.debug('%%%Account Id'+AcctId);
  sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm();
  orgid= UserInfo.getOrganizationId();
  sign = [select id,name from document where name='Honeywell vicepresident sign']; 
  
  //cc = [select id,name,firstname,lastname,Account.Name,Account.Email__c,account.Phone from contact where id=:contractor];
  //system.debug('Invited Contacts'+cc);
  //u = [select id,name,federationIdentifier,contactid,Contact.AccountId,Contact.FirstName,Contact.Lastname,Contact.Name,Contact.Phone,Contact.Email,contact.account.email__c,contact.account.phone,contact.account.name from user where id=:distributor];
  //system.debug('Distributor Contact'+u);
 //  c = [select id,name,(select id,firstname,lastname,email,phone from contacts) from Account where id=:system.label.Honeywell_Account_Executive];
   //system.debug('%%'+c.contacts[0].firstname);
   accnt = [select id,name,Account_Exec_Name__c,Account_Exec_Phone__c,Account_Exec_Email__c from account where id=:AcctId];
            for(contact c1: c.contacts)
            {
             executive_name =accnt .Account_Exec_Name__c;
             executive_email =accnt .Account_Exec_Email__c;
             executive_phone =accnt .Account_Exec_Phone__c ;
            }
  PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
 }

}