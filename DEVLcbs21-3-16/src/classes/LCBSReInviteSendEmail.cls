Global class LCBSReInviteSendEmail{

webservice static void Email(String AccId)
{
    list<Account> AccList = [select id,name,Distributor_Principle__c,Distributor_Principle__r.id,Distributor_Principle__r.email,Account_Executive_Info__c,Account_Exec_Email__c,Account_Exec_Phone__c from Account where id =: AccId];
    EmailTemplate etInviteDis = [Select Id from EmailTemplate where Name = 'LCBS_InviteDistributorTemplate'];
    
    LCBS_InviteDistributorController lcbs = new LCBS_InviteDistributorController();
    lcbs.AccountExeEmail= AccList[0].Account_Exec_Email__c;
    lcbs.AcctId = AccList[0].id;

    
    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.add(AccList[0].Distributor_Principle__r.email);
        
        mail.setToAddresses(sendTo);
        mail.setSenderDisplayName('admin@lcbs.honeywell.com');
        mail.setTargetObjectId(AccList[0].Distributor_Principle__r.id);       
        mail.setTemplateId(etInviteDis.Id); 
        mail.setSaveAsActivity(true);      
        mails.add(mail);
        
        if (!Test.isRunningTest()){
        Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(mails);
           Database.rollback(sp);
             
            List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
            Messaging.sendEmail(lstMsgsToSend);
           }
}
}