/************************************
Name         : LCBS_MyCompanyProfile_Controller
Created By   : Nagarajan Varadarajan
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : This Controller is created for viewing and editing Company Profile also used to add Company Location
Modified On  : Mar 22, 2015
***********************************/
public class LCBS_MyCompanyProfile_Controller {

    
    public Account acct {get; set;}
    public Contact Cont{get;set;} 
    public Account AccountInfo{get; set;}
    
   // public String shippingState {get; set;}
    
   // public String shippingCountry {get; set;} 
  //  public List<Contractor_Branch_Location__c> contractorLocations{get;set;}
    public List<Account> acc {get;set;}
    Public User user{get;set;}
    public List<User> u {get;set;}
    public Company_Location__c contractor {get;set;}
    public string accountId{get;set;}
   // public String PAGE_CSS { get; set; }


   //public String companycountry {get;set;}
   // public String companystate {get;set;}
   // public boolean EditProfilePanel{get;set;}
   // public boolean NewCompanyProfile{get;set;}
    
    public LCBS_MyCompanyProfile_Controller ()
    {
        AccountInfo= new Account();
       // PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
      //  acct = new Account();
        //user=new User();
        //NewCompanyProfile = true;
        //EditProfilePanel = false;
        contractor = new Company_Location__c();        
        accountId ='001g000000TPUEO';
       // accountId  = apexpages.currentpage().getparameters().get('id') ;  
        contractor.Company_Name__c = accountId;
        List<Account> acc = new List<Account>();
        if ( accountId  != null ) { 
      /* acc = [Select Id,name,phone,email__c,OwnerId,
               shippingStreet,(SELECT Contact.FirstName, Contact.LastName FROM Account.Contacts),(Select Name,City__c,Email_Address__c,
                                                                                                  Phone__c,Address__c,State__c,Time_Zone__c,
                                                                                                  Postal_Code__c,Owner__c,Owner_Last_Name__c from Company_Locations__r),shippingCity,shippingState,
               shippingPostalCode,shippingCountry,Time_Zone__c from Account where ownerid=:userinfo.getuserId()];*/
          u =[select id,name,AccountId,account.name,account.id,account.phone,account.email__c,account.State__c,account.Address__c , account.City__c , account.Zip__c , account.Country__c,account.Owner_First_Name__c ,account.Owner_Last_Name__c, account.shippingStreet,account.shippingCity,account.shippingState,account.shippingCountry,account.shippingPostalCode,account.Time_Zone__c,Contact.FirstName, Contact.LastName from User where id=:userinfo.getuserId()];     
             acc = [Select id,name,phone,email__c,State__c,Country__c,shippingStreet,shippingCity,shippingCountry,shippingState,shippingPostalCode,Time_Zone__c,(Select Name,City__c,Email_Address__c,Phone__c,Address__c,State__c,Time_Zone__c,Postal_Code__c,Owner__c,Owner_Last_Name__c from Company_Locations__r),(select FirstName,LastName from Contacts),OwnerId from Account where Id =: u[0].AccountId];
                // NewCompanyProfile = false;
               //  EditProfilePanel = true;
               
               }
        system.debug('###ACCOUNTS'+acc);
        system.debug('###User ID'+userinfo.getuserId());
         system.debug('###User List '+u);
        if(u!=NULL && u.size() > 0)
        {
         user = u[0];
        }
        if(acc != NULL && acc.size() > 0){
                    
            system.debug('In catch 12');
            acct=acc[0];
            system.debug('In catch 23 ');
           if(acc[0].Contacts!=null){
           Cont = acc[0].Contacts;
               }
            system.debug('In catch 909 0');
            //companycountry = shippingCountry;
            //companystate = shippingState;
           // getCountryOption();
          //  getStateOption();
            system.debug('In catch');
        
        }
        else{
            user = new User();
            acct=new Account();
            Cont = new Contact();
        }
    }  
    
    public PageReference insertLocation()
    {
        
        try{        
            insert contractor;
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
        }
        //return Page.LCBS_MyCompanyProfile;
        pagereference p = new pagereference('/apex/LCBS_MyCompanyProfile');
        p.setredirect(true);
        return p;
        
    }
    public pageReference cancelInsertLocation(){
        
        pagereference pa = new pagereference('/apex/LCBS_MyCompanyProfile');
        pa.setredirect(true);
        return pa;
    }
    public PageReference changeCompanyProfile(){
        
        //acct.shippingCountry = companycountry;
        //acct.shippingState = companystate;
        try{        
            update acct;
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
        }
        pagereference p = new pagereference('/apex/LCBS_MyCompanyProfile');
        p.setredirect(true);
        return p;
        
    }
      public PageReference InsertCompanyProfile(){
        
        
        try{        
            insert AccountInfo;
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
        }
        pagereference p = new pagereference('/apex/LCBS_MyCompanyProfile');
        p.setredirect(true);
        return p;
        
    }
    
    public PageReference updateCompany(){
        //return Page.LCBSEditMyCompanyProfile;
        pagereference p = new pagereference('/apex/LCBSEditMyCompanyProfile');
        p.setredirect(true);
        return p;
        
    } 
    public PageReference changeCompanylocation(){
       // return Page.LCBS_NewCompanyLocation;
        pagereference p = new pagereference('/apex/LCBS_NewCompanyLocation');
        p.setredirect(true);
        return p;
    } 
    
    //Added by Nagarajan for Edit Company Profile Location
    
    public List<SelectOption> getCountryOption() {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('--Please Select--','--Please Select--'));
        
        for(LCBS_Country_State__c a : LCBS_Country_State__c.getAll().values()) {
            option.add(new SelectOption(a.Name,a.Name));
        }
        return option;
    }
    
    public List<SelectOption> getStateOption() {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('--Please Select--','--Please Select--'));
        if(user.account.shippingCountry!='--Please Select--'){
        LCBS_Country_State__c countrystate = LCBS_Country_State__c.getInstance(user.account.shippingCountry);
        
        
        
        if(countrystate.state1__c != NULL && countrystate.state1__c != '') {
            for(String s : countrystate.state1__c.split(',')){
                option.add(new SelectOption(s,s));
            }
        }
        if(countrystate.state2__c != NULL && countrystate.state2__c != '') {
            for(String s : countrystate.state2__c.split(',')){
                option.add(new SelectOption(s,s));
            }
        }
        if(countrystate.state3__c != NULL && countrystate.state3__c != '') {
            for(String s : countrystate.state3__c.split(',')){
                option.add(new SelectOption(s,s));
            }
        }
        }
        return option;
    }
    
    
    public PageReference newCompanyProfile() {
        return Page.LCBS_CompanyProfileEntry;
    }

  
    
    
}