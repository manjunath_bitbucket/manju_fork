public class AzureActiveDirectoryHelper
{
    public String getLcbsWebApiAccessToken()
    {
         List<String> requestBodyParams = new String[]{ApplicationConstants.LcbsWebAppResource,ApplicationConstants.LcbsWebAppClientId ,ApplicationConstants.LcbsWebAppClientSecret ,ApplicationConstants.LcbsWebAppGrantTypeAccess };
         String requestBody = String.Format('resource= {0}&client_id={1}&client_secret={2}&grant_type={3}',requestBodyParams);      
         HttpRequest requestToken = new HttpRequest();
         requestToken.setEndpoint(ApplicationConstants.AzureActiveDirectoryAuthEndpoint);
         requestToken.setMethod('POST');
         requestToken.setBody(requestBody);
         Http http = new Http();
         HTTPResponse responseToken = http.send(requestToken);
         System.debug(responseToken.getBody());     
         JSONParser parser = JSON.createParser(responseToken.getBody());
         ActiveDirectoryToken adToken = (ActiveDirectoryToken)parser.readValueAs(ActiveDirectoryToken.class);  
         if(adToken != null)
         {
             String authorizationHeader = adToken.token_type + ' '  + adToken.access_token;
             return authorizationHeader;
         }
         return null;
    }
}